public class TestAccount2 {
    public static void main(String[] args) {
        Account[] arrayofAccounts;
        arrayofAccounts = new Account[5];

        String[] name = {"Peter", "Moz", "Neal", "Sarah", "Elizabeth"};
        double[] balance = {500, 1000, 250, 130, 320};

        for (int i=0; i<arrayofAccounts.length; i++){
            arrayofAccounts[i] = new Account(name[i],balance[i]);
            System.out.println(arrayofAccounts[i].getAccountName() + " has " + arrayofAccounts[i].getBalance());
            arrayofAccounts[i].addInterest();
            System.out.println("New balance: " + arrayofAccounts[i].getBalance());
        }

        //test withdraw
        arrayofAccounts[4].withdraw(300);
        arrayofAccounts[3].withdraw(540);
        arrayofAccounts[0].withdraw();

    }
    
}