public class LeapYears {

    static void myForLoop() {
        int i = 0;
        for (int year = 1900; year < 2000; year++ ){
            if ((year%4) == 0){
                System.out.println(year);
                i++;
                if (i == 5) {
                    break;
                }
            }
        }
        System.out.println("finished");
    }

    static void mySwitchCase() {
        byte gear = 2;
        switch(gear) {
            case 1:
                System.out.println("Under 10 mph");
                break;
            case 2:
                System.out.println("About 20 mph");
                break;
            case 3:
                System.out.println("About 40 mph");
                break;
            case 4:
                System.out.println("Over 50 mph");
        }
    }
    public static void main(String[] args) {
        myForLoop();
        mySwitchCase();
    }
}
    

    
    
    


