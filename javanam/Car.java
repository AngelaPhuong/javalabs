public class Car extends Vehicle{
    public static void main(String[] args) {
        
        //Declare variables
        String make;
        String model;
        double engineSize;
        byte gear;

        //Initialize variables
        make = "Toyota";
        model = "Camry";
        engineSize = 2.2;
        gear = 1;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The gear is " + gear);
        System.out.println("The engine size is " + engineSize);

        //Part 1 Using if/else

        if (engineSize <= 1.3){
            System.out.println("weak car");
        }
        else {
            System.out.println("powerful car");
        }

        System.out.print("Your speed should be ");
        if (gear == 1){
            System.out.print("less than 10mph");
        }
        else {
            System.out.println("Slow down!");
        }

    }

    //@Override
    public void move() {
        System.out.println("Move car");
    }
    
}
