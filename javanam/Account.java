public class Account {
    
	// instance variables
	private String accountName;
	private double balance;

	//contructors
	public Account(String s, double d) {
		accountName = s;
		balance = d;
	}

	//no contructors
	public Account() {
		this("Charles", 50);
    }
    
    //static variable
    private static double interestRate=1.05;

	// instance methods
	public void setBalance(double d)
	{
		balance = d;
	}

	public double getBalance()
	{
		return balance;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String s)
	{
		accountName = s;
	}

	public void addInterest()
	{
		balance = balance *interestRate;
    }
    
    //static method
    public static void setInterestRate(double d){
        interestRate = d;
    }

    //method overloading
    public boolean withdraw(double amount) {
        boolean flag = false;
        if (amount <= balance) {
            balance = balance - amount;
            flag = true;
            System.out.println(amount+ " Withdrawn. New balance: " + balance);
        }
        else 
            System.out.println("Insufficient funds, current balance: " + balance);
        return flag;
    }
    public boolean withdraw() {
        return withdraw(20);
    }

}